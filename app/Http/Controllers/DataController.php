<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request ;
use App\Jobs\SendHttpRequest;

use App\Models\Data;


class DataController extends Controller
{
    public function index() {

        SendHttpRequest::dispatch();

    }

    // Return all
    public function show() {

        $items = Data::all();

        return $items;
    }

    // Return by userId
    public function showByID(Request $request) {

        $items = Data::where('userId', $request->id)->get();

        foreach ($items as $key => $post) {
            $data[$key] = [
                'id' => $post->id,
                'userId' => $post->userId,
                'title' => $post->title,
                'body' => $post->body,
                'created_at' => $post->created_at,
                'updated_at' => $post->updated_at
            ];
        }

        return $data;
    }
}

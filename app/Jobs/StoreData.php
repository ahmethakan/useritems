<?php

namespace App\Jobs;

use App\Models\Data;
use App\Jobs\SendEmail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StoreData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $item;

    // construct çağırılırken datayı al.
    public function __construct($data)
    {
        $this->item = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {

        $query = Data::updateOrCreate([

            'id' => $this->item['id']

        ],
        [
            'userId' => $this->item['userId'],
            'title' => $this->item['title'],
            'body' => $this->item['body']
        ]
        );

        // Eğer yeni oluşturulursa 1 döndürür.
        // 1 dönerse mail at.
        if($query->wasRecentlyCreated) {

            SendEmail::dispatch();

        }

    }
}
